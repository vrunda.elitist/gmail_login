import { Typography } from "@mui/material";

const Paragraph = ({ label, ...props }) => {
    return <Typography {...props}>{label}</Typography>;
  };

export default Paragraph;