import { Button } from "@mui/material";

const MyButton = ({ label, ...props }) => {
    return <Button {...props}>{label}</Button>;
  };

export default MyButton;