import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import React from 'react';
import "../../Styles/Common Styles/DropDown.scss"


const MyDropdown = ({ label, options, ...props }) => {
  return (
    <FormControl variant="standard" className='formcontrol standard'>
      <InputLabel className='label'>{label}</InputLabel>
      <Select {...props}>
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default MyDropdown;