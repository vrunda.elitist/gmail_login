import React from 'react';
import ReactDOM from 'react-dom/client';
import { createBrowserRouter, defer, RouterProvider, Outlet} from "react-router-dom";
import './index.css';
import reportWebVitals from './reportWebVitals';
import CreateAccount from './Pages/CreateAccount/CreateAccount.js';
import ForgetEmail from './Pages/ForgetEmail/Forgetemail.js';
import Loader from './Pages/Home/Loader.js';

function Layout(){
  return(
    <>
    <Loader/>
    <Outlet/>
    </>
  )
}
const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    loader: async ({ request }) => {

      const results = fetch("https://countriesnow.space/api/v0.1/countries/population/cities", {
        headers: {
          "x-rapidmock-delay": 90000
        }
      }).then((response) => response.json());

      return defer({ results });

    },
  },
  {
    path: "/account",
    element: <CreateAccount />
  },
  {
    path: '/forgotemail',
    element: <ForgetEmail />
  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
