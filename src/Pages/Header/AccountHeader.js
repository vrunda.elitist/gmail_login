import { Box } from "@mui/material"
import Paragraph from "../../Components/Paragraph/Paragraph"
import "../../Styles/Common Styles/Header.scss"
function Header() {
    return (
        <>
            <Box className="hero">
                <img className="image" src="./Logo.png" alt=""></img>
                <Paragraph className="heading" label="Create a Google Account" />
                <Paragraph className="sub-heading" label="Enter your name" />
            </Box>
        </>
    )
}

export default Header