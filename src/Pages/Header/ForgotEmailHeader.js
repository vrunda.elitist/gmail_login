import { Box } from "@mui/material"
import Paragraph from "../../Components/Paragraph/Paragraph"
import "../../Styles/Common Styles/Header.scss"
function Header() {
    return (
        <>
            <Box className="hero">
                <img className="image" src="./Logo.png" alt=""></img>
                <Paragraph className="heading" label="Find your email" />
                <Paragraph className="sub-heading" label="Enter your phone number or recovery email" />
            </Box>
        </>
    )
}

export default Header