import { Box } from "@mui/material"
import Paragraph from "../../Components/Paragraph/Paragraph"
import "../../Styles/Common Styles/Header.scss"
function Header() {
    return (
        <>
            <Box className="hero">
                <img className="image" src="./Logo.png" alt=""></img>
                <Paragraph className="heading" label="Sign in" />
                <Paragraph className="sub-heading" label="Use your Google Account" />
            </Box>
        </>
    )
}

export default Header