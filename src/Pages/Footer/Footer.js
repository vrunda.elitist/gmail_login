import { Box } from "@mui/material";
import MyDropdown from "../../Components/DropDown/DropDown";
import "../../Styles/Common Styles/Footer.scss"
import Paragraph from "../../Components/Paragraph/Paragraph";

function Footer() {
    const languageOptions = [
        { value: 'en', label: 'English' },
        { value: 'es', label: 'Spanish' },
        { value: 'fr', label: 'French' },
    ];
    return (
        <>
        <Box className='footer'>
            <Box className='lang'>
                <MyDropdown label="Select Language" options={languageOptions} className="dropdown" />
            </Box>
            <Box className='services'>
                <Paragraph label="Help"/>
                <Paragraph label="Privacy"/>
                <Paragraph label="Terms"/>
            </Box>
        </Box>
    </>
    )
}

export default Footer