import { Box,FormControl } from "@mui/material"
import Header from "../Header/ForgotEmailHeader"
import MyTextField from "../../Components/TextField/TextField"
import MyButton from "../../Components/Button/Button"
import Footer from "../Footer/AccountFooter"
import "../../Styles/Common Styles/ForgotEmail.scss"


function ForgetEmail() {
    return(
        <>
         <Box className="container">
                    <Header />
                    <FormControl className="form">
                        <MyTextField className="text" label="Phone number or email" variant="outlined" />
                        <Box className='buttons'>
                            <MyButton className='buttn' label="Next" href="https://mui.com/material-ui/react-button/" variant="contained" size="medium" />
                        </Box>
                    </FormControl>
                </Box>
                <Footer />
        </>
    )
}

export default ForgetEmail