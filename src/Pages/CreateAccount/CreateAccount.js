import { Box, FormControl } from '@mui/material';
import Header from '../Header/AccountHeader.js';
import MyTextField from '../../Components/TextField/TextField';
import MyButton from '../../Components/Button/Button';
import Footer from '../Footer/AccountFooter.js';
import "../../Styles/Common Styles/CreateAccount.scss"


function CreateAccount(){
    return(
        <>
          <Box className="container">
                    <Header />
                    <FormControl className="form">
                        <MyTextField className="text" label="First name" variant="outlined" />
                        <MyTextField className="text" label="Last name(optional)" variant="outlined" />
                        <Box className='buttons'>
                            <MyButton className='button' label="Next" href="https://mui.com/material-ui/react-button/" variant="contained" size="medium" />
                        </Box>
                    </FormControl>
                </Box>
                <Footer />
        </>
    )
}

export default CreateAccount