import React, {Suspense } from 'react';
import { CircularProgress, Box } from '@mui/material';
import { useLoaderData, Await } from "react-router-dom";
import Home from './Home';


const Loader = () => {

const { results } = useLoaderData();

  return (
    <Suspense fallback={<Box display="flex" justifyContent="center" alignItems="center" height="100vh"><CircularProgress /></Box>}>
      <Await
        resolve={results}
        errorElement={<p>Error loading data</p>}

      >
       {<Home/>}
      </Await>
    </Suspense>
  );
};


export default Loader;


