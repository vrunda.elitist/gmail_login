import { createTheme, ThemeProvider } from '@mui/material/styles'
import { Box, FormControl } from '@mui/material';
import { Link } from 'react-router-dom';
import MyButton from '../../Components/Button/Button'
import MyTextField from '../../Components/TextField/TextField';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Paragraph from '../../Components/Paragraph/Paragraph';
import "../../Styles/Common Styles/Home.scss"




const theme = createTheme({
    typography: {

        h3: {
            marginRight: '275px',
            marginTop: "10px",
            color: 'rgb(26, 115, 232)',
            borderRadius: '4px',
            display: 'inline-block',
            fontWeight: 500,
            letterSpacing: '0.25px',
            outline: 0,
            cursor: 'pointer',
            fontSize: '14px',
           

          
        },

        body1: {
            fontSize: '14px',
            marginRight: '12px',
            marginTop: '45px',
            color: '#766d6d'
        },

        h4: {
            marginRight: '295px',
            color: 'rgb(26, 115, 232)',
            borderRadius: '4px',
            display: 'inline-block',
            fontWeight: '500',
            letterSpacing: '0.25px',
            outline: '0',
            cursor: 'pointer',
            fontSize: '14px',
            marginTop: '4px',
        }

    },
});

function Home() {
    return (
        <>
            <ThemeProvider theme={theme}>
                <Box className="container">
                    <Header />
                    <FormControl className="form">
                        <MyTextField className="textfield" label="Email or Phone" variant="outlined" />
                        <Link to="/forgotemail">
                        <Paragraph variant="h3" label="Forgot email?" />
                        </Link>
                        <Paragraph className="label" label="Not your computer? Use Guest mode to sign in privately." />
                        <Paragraph className="label" variant="h4" label="Learn more" />
                        <Box className='buttons'>
                            <Link to="/account">
                            <MyButton className='btn' label="Create account" variant="text"/>
                            </Link>
                            <MyButton className='btn2' label="Next" href="https://mui.com/material-ui/react-button/" variant="contained" size="medium" />
                        </Box>
                    </FormControl>
                </Box>
                <Footer />
            </ThemeProvider>
        </>

    )
}

export default Home

